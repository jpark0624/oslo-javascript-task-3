const cheerio = require('cheerio');
const request = require('request');
const fs = require('fs');

request('https://store.steampowered.com/app/1100600/Football_Manager_2020/', (error, response, html) => {
    if(!error && response.statusCode == 200) {
        const $ = cheerio.load(html);

        // Scraping name and description from the webiste
        const gameName = $('.apphub_AppName').text();
        const gameDescription = $('.game_description_snippet').text().trim();

        // Parsing data to json
        const gameInfo = JSON.parse(`{"title":"${gameName}", "description":"${gameDescription}"}`); 
        const jsonFile = JSON.stringify(gameInfo);

        // Sending data to the gameInfo.json file
        fs.writeFile('gameInfo.json', jsonFile, 'utf-8', (error) => {
            if(error)
                console.log(error.message);
                console.log('Json data saved to the gameInfo.json file');
        });
    }
})

